/*
	Parse formul
*/

window.FormulCalculation = function(input){
	var def = {
		formula:"",
		_fields:""
	},
	self = this;
	input = (input)?input:{};

	for (property in input) {
		def[property] = input[property];
	};

	var MathFunction = {
		multiply:function(args){
			args = args.replace(/(((\+|-)?((\d+(\.\d+)?)|(\.\d+)))\*((\+|-)?((\d+(\.\d+)?)|(\.\d+))))/gi,function(arr){
				var find = arr.match(/(\+|-)?((\d+(\.\d+)?)|(\.\d+))/gi),
					result;
					for(var i in find){
						result = ((typeof(result)=="undefined")||(typeof(result)=="NaN"))?parseFloat(find[i]):parseFloat(result)*parseFloat(find[i]);
					}
				return result;
			});
			return args;
		},
		division:function(args){
			args = args.replace(/(((\+|-)?((\d+(\.\d+)?)|(\.\d+)))\/((\+|-)?((\d+(\.\d+)?)|(\.\d+))))/gi,function(arr){
				var find = arr.match(/(\+|-)?((\d+(\.\d+)?)|(\.\d+))/gi),
					result;
					for(var i in find)
					{
						result = ((typeof(result)=="undefined")||(typeof(result)=="NaN"))?parseFloat(find[i]):parseFloat(result)/parseFloat(find[i]);
					}

				return result;
			});
			return args;
		},
		addition:function(args){
			var calc = "";
			args = args.replace(/(((\+|-)?((\d+(\.\d+)?)|(\.\d+)))\+((\+|-)?((\d+(\.\d+)?)|(\.\d+))))/gi,function(arr){
				calc = arr;
				var find = arr.match(/(\+|-)?((\d+(\.\d+)?)|(\.\d+))/gi),
					result;
					for(var i in find){
						result = ((typeof(result)=="undefined")||(typeof(result)=="NaN"))?parseFloat(find[i]):parseFloat(result)+parseFloat(find[i]);
					}
				return result;
			});

			if(args.match(/\+/gi)){
				args = MathFunction.addition(args)
			}

			return args;
		},
		subtraction:function(args){
			args = args.replace(/(((\+|-)?((\d+(\.\d+)?)|(\.\d+)))-((\+|--)?((\d+(\.\d+)?)|(\.\d+))))/gi,function(arr){
				var find = arr.match(/(\+|--)?((\d+(\.\d+)?)|(\.\d+))/gi),
					result;
					for(var i in find){
						result = ((typeof(result)=="undefined")||(typeof(result)=="NaN"))?parseFloat(find[i]):parseFloat(result)-parseFloat(find[i]);
					}
				return result;
			});
			
			if(args.match(/-/gi)){
				args = MathFunction.subtraction(args)
			}

			return args;
		},
		log:function(args){
			
			return args;
		},
		sin:function(args){

			return args;
		}
	}

	var pMethod = {
		map:function( formula )
		{
			var reg  = /\(((?:([^\(\)]+)))\)/gi;
			console.log("action="+formula);

			if(formula){
				for(var index in MathFunction){
					var regMath = new RegExp(index+"\(((?:([^\(\)]+)))\)","gi")
					formula = formula.replace(regMath,index+"[$1]");
				}

				if(formula.match(reg)){
					var find = formula.match(reg);
					for (var index in formula.match(reg) ){
						var result = pMethod.mathOperation(find[index]);
							formula = formula.split(find[index]).join(result);
					};
					
					return pMethod.map(formula);
				}else{
					
					return pMethod.mathOperation(formula);
				}
				
				return 2;
			}
			return 0; 

		},
		mathOperation:function( formula )
		{
			console.log("subaction="+formula);
			// console.log("preformula="+formula)
			formula = formula.replace(/([\(\)])/gi,"");
			formula = formula.replace(/(--)/gi,"+");
			// console.log("postformula="+formula)

			for(var i in def._fields)
			{
				var tag  = def._fields[i].$el.data("tag");
					if(tag){
						formula = formula.replace(new RegExp(tag+"\\b","gm"),(def._fields[i].$el.val())?def._fields[i].$el.val():0)
					}
			}
			console.log("calculate="+formula)
			for(var i in MathFunction){
				formula = MathFunction[i](formula);
			}	
			return formula;
		}


	}

	this.setPropertys = function( obj )
	{
		for (	property in obj	) 
		{
			def[property] = obj[property];
		};

		return 1;
	}
	
	this.calculate = function()
	{
		
		if(	def.formula )
		{
			return pMethod.map(def.formula);
		}

		return 0;
	}

	return this;
};

/*
	Base class
*/
(function($){
	$.fn.calculate = function( options )
	{

		var self 	=	this,
			def 	= 	$.extend({
				fields:{},
				output:"",
				aroundNumber:2,
				formula:""
			},options),
			Calculation = "";
		
		def._fields = {};
		

		var privateMethod = {

			searchField:function()
			{
				if(Object.keys(def.fields).length){
					$.each(def.fields,function(key,val){
						var find = $(key,self);
						if(find.length && find.length == 1){
							def._fields[key] = {
								"$el":find,
								"key":key,
								"type":val
							}
						}else{
							 throw "Not found field with name = "+key;
						}
					})
				}else{
					throw "Not found fields";
				}
			},

			controlType:function( key )
			{
				if(def._fields[key]){
					var val = def._fields[key].$el.val();
						val = val.replace(",", ".").replace(" ","").replace(/[^\d|.]/gi, '');
						val = (val)?0:val;
					
					switch(def._fields[key].type){
						case "float":
							// def._fields[key].$el.val(parseFloat(val).toFixed(def.aroundNumber));
						break;
						
						case "int":
							// def._fields[key].$el.val(parseInt(val));
						break;

						default:
							throw "not correct type";
						break;
					}
				}else{	
					throw "not found field";
				}
			},

			setEventsField:function()
			{
				if(Object.keys(def._fields).length)
				{
					$.each(def._fields,function(key,val)
					{
						val.$el.on("change",function(){

							privateMethod.controlType(key);
						
						})
					})

				}else{

					throw "Not found fields";
				
				}
			},

			calculate:function( input )
			{
				Calculation.setPropertys(input);
				return Calculation.calculate();
			}
		}
		
		var publicMethod = {
			calc:function( formula )
			{
				
				if(formula)
				{
					return privateMethod.calculate($.extend(def,{"formula":formula}));
				}

				return privateMethod.calculate(def);
			}
		}

		this.init = function()
		{
			try
			{
				privateMethod.searchField(); // search field for calculation
				privateMethod.setEventsField(); // set events for fields
				Calculation = window.FormulCalculation(def);
			}
			
			catch(e)
			{
				console.log(e);
			}
			
			finally
			{
				$.each(publicMethod,function(key,val){
					self[key] = val;
				})
			}
		}

		this.init();
		
		return this;
	}
})(jQuery)