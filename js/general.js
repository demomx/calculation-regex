$(function(){
	var calc = $("#calc").calculate({
		fields:{
			"[name='base']":"int",
			"[name='adv']":"int",
			"[name='advK']":"int",
			"[name='time']":"int",
			"[name='baseP']":"int",
			"[name='timeK']":"int"
		},
		formula:"base+(adv-(adv*(advK/100)))+(time*baseP-((time*baseP)*(timeK/100)))"
	});

	// 
	// Не правильно парсится тег
	// 
	// select не обрабатывается входное значение 192
	// +(time*baseP-((time*baseP)/100)*timeK)
	
	$("[name='adv']").on("change",function(){
		var k = $("[name='avKd']"),
			base = $("[name='base']");
			switch($(this).val()){
				case "0":
					k.find("option").filter(function() {
					    return $(this).val() == '0'; 
					}).attr('selected', true);
				break;
				case "6000":
					k.find("option").filter(function() {
						var self = this;
						switch($(base).val()){
							case "0": return $(self).val() == '0'; 
							case "15300": return $(self).val() == '50'; 
							case "25700": return $(self).val() == '50'; 
							case "40500": return $(self).val() == '100'; 
						} 
					}).attr('selected', true);
				break;
				case "11700":
					k.find("option").filter(function() {
						var self = this;
						switch($(base).val()){
							case "0": return $(self).val() == '0'; 
							case "15300": return $(self).val() == '25'; 
							case "25700": return $(self).val() == '50'; 
							case "40500": return $(self).val() == '50'; 
						} 
					}).attr('selected', true);
				break;	
				case "21700":
					k.find("option").filter(function() {
						var self = this;
						switch($(base).val()){
							case "0": return $(self).val() == '0'; 
							case "15300": return $(self).val() == '10'; 
							case "25700": return $(self).val() == '25'; 
							case "40500": return $(self).val() == '25'; 
						} 
					}).attr('selected', true);
				break;
			}
	})
	$("[name='base']").on("change",function(){
		var self = this;
			$("[name='baseP']").find("option").filter(function() {
				var self1 = this;
				switch($(self).val()){
					case "0": return $(self1).val() == '0'; 
					case "15300": return $(self1).val() == '2000'; 
					case "25700": return $(self1).val() == '1500'; 
					case "40500": return $(self1).val() == '1500'; 
				} 
			}).attr('selected', true);
	});
	
	$("[name='time']").on("change",function(){
		var self = this;
			$("[name='timeK']").find("option").filter(function() {
				var self1 = this;
				switch($(self).val()){
					case "0": return $(self1).val() == '0'; 
					case "6": return $(self1).val() == '25'; 
					case "12": return $(self1).val() == '50';
				} 
			}).attr('selected', true);
	});

	$("#calcB").on("click",function(e){
		// alert(calc.calc())
		$("#result").html(calc.calc())
		e.preventDefault();
	})
	// console.log(calc.calc());
})